TAG_KERNEL="kernel-genkernel-x86"
TAG_FILES="$TAG_KERNEL " # initramfs-genkernel-x86 System.map-genkernel-x86"
BOOT_DIR="/boot"
TMP="/tmp"
MARK="auto"
VALUE=""

_NEW_KERNEL="kernel-genkernel-x86-3.0.0.$MARK"


function log()
{
        echo "VALUE: $VALUE - $1"
}

function dereference()
{
	VALUE=`ls -l "$1" | sed 's/.*-> \(.*\)/\1/'`
	log "dereference($i)"
}

function backup()
{
        FROM="$1"
        TO="$2"


        for i in $TAG_FILES; do
                log "cp -f $i.$MARK.$FROM $i.$MARK.$TO backup()"
                cp -f $i.$MARK.$FROM $i.$MARK.$TO
        done
}

# select the last compiled kernel
function select_first()
{
	for f in $TAG_FILES; do
		VALUE=`ls -1t $BOOT_DIR/$f-* | head -1`
		ln -sf $VALUE $f.$MARK.$1

		log "created link $f.$MARK.$1 -> $VALUE"
	done
}

# select the last kernel with a different version (or name)
function select_second()
{
	for f in $TAG_FILES; do
		VALUE=`ls -1t $BOOT_DIR/$f-* | head -2 | tail -1`
		ln -sf $VALUE $f.$MARK.$1

		log "created link $f.$MARK.$1 -> $VALUE"
	done
}

# select a specific version
# arg1 version (suffix)
# arg2 tag
function select_version()
{
	for f in $TAG_FILES; do
		VALUE=$f$1
		ln -sf $VALUE $f.$MARK.$2

		log "created link $f.$MARK.$2 -> $VALUE"
	done
}

function stable_fn()
{
	dereference $TAG_KERNEL.$MARK.stable
	ACTUAL=$VALUE
	cd $BOOT_DIR
	CANDIDATES=`ls $TAG_KERNEL*|grep -v $MARK`
	LIST_CAND=""
	for i in $CANDIDATES; do 
		if [ "$i" == "$ACTUAL" ]; then
			LIST_CAND="$LIST_CAND true "
		else
			LIST_CAND="$LIST_CAND false "
		fi
		LIST_CAND="$LIST_CAND $i "
	done
	VALUE=`$ZENITY --list --radiolist --column=" " --column "kernel" --text "select the stable kernel" $LIST_CAND | sed "s/$TAG_KERNEL\(.*\)/\1/"`
	select_version $VALUE stable
}

function stable_fn()
{
	cd $BOOT_DIR
	echo "looking $TAG_KERNEL.$MARK.stable"
	ls $TAG_KERNEL.$MARK.stable
	dereference $TAG_KERNEL.$MARK.stable
	ACTUAL=$VALUE
	echo "ACTUAL: $ACTUAL"
	CANDIDATES=`ls $TAG_KERNEL*|grep -v $MARK`
	LIST_CAND=""
	for i in $CANDIDATES; do 
		if [ "$i" == "$ACTUAL" ]; then
			LIST_CAND="$LIST_CAND true "
		else
			LIST_CAND="$LIST_CAND false "
		fi
		LIST_CAND="$LIST_CAND $i "
	done
	VALUE=`$ZENITY --list --radiolist --column=" " --column "kernel" --text "select the stable kernel" $LIST_CAND | sed "s/$TAG_KERNEL\(.*\)/\1/"`
	select_version $VALUE stable
}
