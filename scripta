#!/bin/bash
# Copyright 2009, Roberto Previdi
# GPL v2 or later

DEBUG=1

export ZENITY="zenity --width=800 --height=400"
export MSGBOX="$ZENITY --info --text "
export ERROR="$ZENITY --error --text "
export ENTRY="$ZENITY --entry --text "
export EDITFILE="$ZENITY --text-info --editable --filename "
export TMP="/tmp"
USER_MENU_DIR="$HOME/.scripta/menu"
SYS_MENU_DIRS="/usr/share/scripta/menu"
CMD_MENU_DIRS=""
MENU_DIRS="$USER_MENU_DIR"
# $SYS_MENU_DIRS
export SEP="==================="
TITLE="scripta"


echo $0
pwd

# arg1: relative/absolute path to switch to
function change_dir()
{
	echo "change_dir($1)"
	cd $1
	MENU_DIR=$(pwd)
}

function new_menu()
{
	NAME="$($ENTRY "enter the name of the menu" | sed 's/ /_/')"
	if [ $? -gt 0 ]; then
		$MSGBOX operation cancelled
	else
		mkdir "$NAME"
	fi
}

function new_script()
{
	NAME="$($ENTRY "enter the name of the script"| sed 's/ /_/')"
	if [ $? -gt 0 ]; then
		$MSGBOX operation cancelled
	else
		touch "$NAME"
#	--->	#	
		cat >> "$NAME" << _END
function run()
{
	\$MSGBOX "edit me!"
}
_END
#	--->	#
	fi
}

#arg1: the file/dir to rename
function rename_file()
{
	NEW_NAME=$($ZENITY --entry --text "enter new name" --entry-text $1)
	if [ $? -gt 0 ]; then
		$MSGBOX operation cancelled
	else
		mv $1 $NEW_NAME
	fi
}

#arg1: menu to edit
function edit_menu()
{
	ACTION=$($ZENITY --column "Select action" --list rename remove)
	if [ $? -gt 0 ]; then
		$MSGBOX operation cancelled
	elif [ "$ACTION" == "remove" ]; then
		PASSWORD=$($ZENITY --entry --text "enter \"DELETE this MENU\"" --entry-text "pleeease, don't kill meeee")
		if [ $? -gt 0 -o $PASSWORD != "DELETE this MENU"]; then
			$MSGBOX "operation cancelled"
		else
			rm -rf $1
		fi
	elif [ "$ACTION" == "rename" ]; then
		rename_file $1
	fi	
}

#arg1: file to edit
function edit_file()
{
	ACTION=$($ZENITY --column "Select action" --list edit rename remove)
	if [ $? -gt 0 ]; then
		$MSGBOX "operation cancelled"
	elif [ "$ACTION" == "edit" ]; then
		$EDITFILE $1 --title $1 > $1.tmp
		mv -f $1.tmp $1
	elif [ "$ACTION" == "remove" ]; then
		PASSWORD="$($ZENITY --entry --text "enter \"DELETE this FILE\"" --entry-text "noooo, save meeee")"
		if [ $? -gt 0 -o $PASSWORD != "DELETE this FILE"]; then
			$MSGBOX "operation cancelled"
		else
			rm -rf $1
		fi
		
	elif [ "$ACTION" == "rename" ]; then
		rename_file $1
	fi
}

#edit the menus in the current path
function edit_fn()
{	
	while [ 1 ]; do
		TITLE="scripta - EDIT"
		menu_items --desc-prefix "edit--" back new_submenu new_script quit

		if [ "$SELECTED" == $SEP ]; then
			do_nothing
		elif [ "$SELECTED" == "back" ]; then
			return 0
		elif [ "$SELECTED" == "new_submenu" ]; then
			new_menu
		elif [ "$SELECTED" == "new_script" ]; then
			new_script
		elif [ -d $SELECTED ]; then
			edit_menu $SELECTED
		elif [ -f $SELECTED ]; then
			edit_file $SELECTED	
		fi
	done

}

# arg1: bash script which defines a "run" function
function execute()
{
	NEED_SUDO="no"	
	
	function run()
	{
		$ERROR "Error, the file doesn't define a run() function"
	}
	
	source $1
#	if [ $NEED_SUDO == "yes" ]; then	
#		SUDO_CMD="sudo -E"
#	else
#		SUDO_CMD=""
#	fi
#	export -f run
#	$SUDO_CMD ...
	run
	
}

function do_nothing()
{
	DUMMY=$DUMMY
}

# arg1: return value of zenity
# arg2: selected command
function parse_menu_commons()
{
	if [ $1 -gt 0 ]; then
		echo exiting with status $1
		exit $1
	fi

	if [ "$2" == "quit" ]; then
		exit 0
	fi
}

function escape()
{
	VALUE=$(echo $1 | sed 's/[/]/\\\//g')
}

#arg1: prefix for the description part
#arg2: absolute path of choosen root
function get_items()
{
# per eliminare i path				sed 's/.*\/\([^/]*\)/\1/' | 
# per eliminare i caporiga			sed -e :a -e '$ P; /$/N; s/\n/ /; ta;'| 
# tiene solo la prima riga			head -n 1 | 
# aggiunge uno spazio alla fine			sed 's/$/ /' | 
# duplica e modifica				sed "s/\([a-z]\+\) /\1 $1\1... /g"
	escape $2
	local ROOT="$VALUE"
	SUBMENUS="$SUBMENUS \
		$(
			find `pwd`/ -maxdepth 1 -mindepth 1 -type d | 
				grep -v ".*~" |
				sort |
				sed -e :a -e '$ P; /$/N; s/\n/ /; ta;'| 
				head -n 1 | 
				sed 's/$/ /' | 
				sed "s/\([/_a-z]\+\) /\1 $1\1... $ROOT /g"
		)"

	MENU_ITEMS="$MENU_ITEMS \
		$(
			find `pwd`/ -maxdepth 1 -mindepth 1 -type f | 
				grep -v ".*~" |
				sort |
				sed -e :a -e '$ P; /$/N; s/\n/ /; ta;'| 
				head -n 1 |
				sed 's/$/ /' | 
				sed "s/\([/_a-z]\+\) /\1 $1\1 $ROOT /g"
		)"

}

# arg1-n: additional menu entries
function menu_items()
{
	echo "menu_items()"
	ADDITIONAL=""
	DESC_PREFIX=""
	ENABLE_OVERRIDE=""
	while [ $# -gt 0 ]; do
		if [ "$1" == "--desc-prefix" ]; then
			shift 
			DESC_PREFIX="$1"
		elif [ "$1" == "--enable-override" ]; then
			ENABLE_OVERRIDE="TRUE"
		else
			ADDITIONAL="$ADDITIONAL $1 $1 $1"
		fi
		shift
	done
	
	SUBMENUS=""
	MENU_ITEMS=""	

	for ACTUAL_ROOT in $MENU_DIRS; do
		change_dir "$ACTUAL_ROOT/$RELATIVE_PATH"

		get_items "$DESC_PREFIX" $ACTUAL_ROOT
	done
	
	ITEMS="$SUBMENUS $MENU_ITEMS"
		
	if [ "$ENABLE_OVERRIDE" == "TRUE" ]; then
		ITEMS_ARRAY=( $ITEMS )
		DESC_ARRAY=( )

		for i in $(seq 0 $( let "v=${#ITEMS_ARRAY[@]} / 3 - 1"; echo $v)); do
			DESC_ARRAY[$i]=$ITEMS_ARRAY[$( let "v=${i} * 3 + 1"; echo $v)]
		done

		for i in $(seq 0 $( let "v=${#DESC_ARRAY[@]} - 1"; echo $v)); do
			# HEAD is ${DESC_ARRAY[@]:0:$i}
			if [ $DESC_ARRAY[$i] == "----" ]; then
				continue
			fi
			DESC_TAIL=( ${DESC_ARRAY[@]:$( let "v=$i + 1"; echo $v)} )
			DESC_TAIL=( ${DESC_TAIL[@]//${DESC_ARRAY[$i]}/----} )
			DESC_ARRAY=( ${DESC_ARRAY[@]:0:$i} ${DESC_TAIL[@]} )
		done
		
		for i in $(seq 0 $( let "v=${#ITEMS_ARRAY[@]} / 3 - 1"; echo $v)); do
			if [ $DESC_ARRAY[$i] == "----" ]; then
				ITEMS_ARRAY[$( let "v=${i}*3"; echo $v)]=""
				ITEMS_ARRAY[$( let "v=${i}*3+1"; echo $v)]=""
				ITEMS_ARRAY[$( let "v=${i}*3+2"; echo $v)]=""
			fi
		done
		
		ITEMS=${DESC_ARRAY[@]}
	fi
		

	SELSTRUCT=( $($ZENITY --title "$TITLE"  --print-column ALL --column Command --column Operation --column Root --list $ITEMS $SEP $SEP $SEP $ADDITIONAL | sed 's/|/ /g') )

	echo "SELSTRUCT=${SELSTRUCT[@]}"
	SELECTED=${SELSTRUCT[0]}
	escape ${SELSTRUCT[2]}
	local ROOT=$VALUE
	echo "ROOT=$ROOT"
	echo "SELECTED=$SELECTED"
	RELATIVE_PATH=$(	if [ -f $SELECTED ]; then dirname $SELECTED; elif [ -d $SELECTED ]; then echo $SELECTED; else echo ""; fi | sed "s/$ROOT\/\(.*\)$/\1/")
	echo "RELATIVE_PATH=$RELATIVE_PATH"
	parse_menu_commons $? "$SELECTED"
}

function parse_options()
{
	while [ $# -gt 0 ]; do
		#if [ "$1" == "--menu-dir" -o "$1" == "-m" ]; then
			#shift 
			#CMD_MENU_DIRS="$CMD_MENU_DIRS $1"
		#elif [ "$1" == "--menu-dir" -o "$1" == "-" ]; then
			# implement actions
		#fi

		shift
	done

}


#parse_options

while [ 1 ]; do


	TITLE="scripta"
	menu_items back edit quit

	if [ "$SELECTED" == $SEP ]; then
		do_nothing
	elif [ "$SELECTED" == "edit" ]; then
		edit_fn
	elif [ "$SELECTED" == "back" ]; then
		change_dir ..
	elif [ -d $SELECTED ]; then
		echo "Entering in $SELECTED"
		change_dir $SELECTED
	elif [ -f $SELECTED ]; then
		echo "Executing $SELECTED"
		execute $SELECTED
	fi
done
